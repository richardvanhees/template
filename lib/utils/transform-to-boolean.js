"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (val) {
    if (val === 'true')
        return true;
    if (val === 'false')
        return false;
    return val;
});
