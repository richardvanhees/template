"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = require("chalk");
var modules_1 = require("../modules");
var transform_to_boolean_1 = __importDefault(require("./transform-to-boolean"));
exports.default = (function (config, opts) {
    var options = opts || {};
    var configKeys = Object.keys(config);
    configKeys.forEach(function (key, i) {
        modules_1.settings.setValue(key, (0, transform_to_boolean_1.default)(config[key]), false);
        !options.isReset && console.log("".concat((0, chalk_1.blue)(key), " is now ").concat((0, chalk_1.red)(config[key])));
        if (i + 1 === configKeys.length)
            console.log('');
    });
    options.isReset && console.log("The ".concat((0, chalk_1.blue)('default values'), " have been restored."));
});
