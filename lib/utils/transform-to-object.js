"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (arr) {
    var newObj = {};
    arr.forEach(function (key, i) {
        if (!(i % 2)) {
            newObj[key] = arr[i + 1];
        }
    });
    return newObj;
});
