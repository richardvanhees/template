"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isUsingTypeScript = exports.isUsingJss = exports.isUsingRedux = exports.isUsingHooks = exports.commandUsed = void 0;
var modules_1 = require("../modules");
var set_defaults_1 = __importDefault(require("../set-defaults"));
var get_command_1 = __importDefault(require("../get-command"));
// Init
modules_1.settings.init({ appName: 'React Template Engine', reverseDNS: 'com.bar.foo' });
(0, set_defaults_1.default)();
modules_1.settings.setValue('commandUsed', (0, get_command_1.default)());
exports.commandUsed = modules_1.settings.value('commandUsed');
exports.isUsingHooks = modules_1.settings.value('hooks');
exports.isUsingRedux = modules_1.settings.value('redux');
exports.isUsingJss = modules_1.settings.value('jss');
exports.isUsingTypeScript = modules_1.settings.value('typescript');
