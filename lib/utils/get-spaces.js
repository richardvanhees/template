"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (obj, currentKey) {
    var longestKey = Object.keys(obj).reduce(function (total, key) {
        if (key.length > total)
            return key.length;
        else
            return total;
    }, 0);
    var spaceCount = (longestKey + 2) - currentKey.length;
    var spaces = '';
    for (var i = 0; i < spaceCount; i++) {
        spaces += ' ';
    }
    return spaces;
});
