"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var modules_1 = require("../modules");
exports.default = (function (query) {
    var confirm = new modules_1.Confirm(query);
    return new Promise(function (resolve) {
        confirm.ask(function (answer) { return resolve(answer); });
    });
});
