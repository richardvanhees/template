"use strict";
// Default config
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    hooks: false,
    jss: false,
    redux: false,
    TypeScript: false,
};
