"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Confirm = exports.settings = void 0;
// @ts-nocheck
var settings_store_1 = __importDefault(require("settings-store"));
exports.settings = settings_store_1.default;
var prompt_confirm_1 = __importDefault(require("prompt-confirm"));
exports.Confirm = prompt_confirm_1.default;
