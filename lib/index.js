#!/usr/bin/env node --harmony
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var fs_1 = require("fs");
var child_process_1 = require("child_process");
var minimist_1 = __importDefault(require("minimist"));
var change_case_1 = require("change-case");
var chalk_1 = require("chalk");
var file_packages_1 = __importDefault(require("./file_packages"));
var cli_funcs_1 = require("./cli-funcs");
var get_settings_1 = require("./utils/get-settings");
var options = (0, minimist_1.default)(process.argv.slice(2));
var initTemplateModule = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!(options.list || options.ls || (options.l && options.s))) return [3 /*break*/, 2];
                return [4 /*yield*/, (0, cli_funcs_1.listFunc)()];
            case 1:
                _a.sent();
                _a.label = 2;
            case 2:
                if (!(options.help || options.h)) return [3 /*break*/, 4];
                return [4 /*yield*/, (0, cli_funcs_1.readmeFunc)()];
            case 3:
                _a.sent();
                _a.label = 4;
            case 4:
                if (!(options.version || options.v)) return [3 /*break*/, 6];
                return [4 /*yield*/, (0, cli_funcs_1.versionFunc)()];
            case 5:
                _a.sent();
                _a.label = 6;
            case 6:
                if (!(options.config || options._[0] === 'config')) return [3 /*break*/, 8];
                return [4 /*yield*/, (0, cli_funcs_1.configFunc)()];
            case 7:
                _a.sent();
                _a.label = 8;
            case 8: return [2 /*return*/, new Promise(function (resolve, reject) {
                    var type = options._[0];
                    var name = options._[1];
                    if (!options._.length)
                        reject("\nNo arguments found. Run ".concat((0, chalk_1.blue)("".concat(get_settings_1.commandUsed, " --help")), " for more information.\n"));
                    if (!name)
                        reject("\nUsage: ".concat((0, chalk_1.blue)("".concat(get_settings_1.commandUsed, " <template name> <element name>")), "\nRun ").concat((0, chalk_1.blue)("".concat(get_settings_1.commandUsed, " --help")), " for more information.\n"));
                    if (!file_packages_1.default[type])
                        reject("\nTemplate ".concat(type, " does not exist.\nRun ").concat((0, chalk_1.blue)("".concat(get_settings_1.commandUsed, " -ls")), " for all templates.\n"));
                    if (get_settings_1.isUsingHooks && !file_packages_1.default[type].hasHooks)
                        reject("\nTemplate ".concat(type, " does not support hooks.\n"));
                    resolve({ type: type, name: name });
                })];
        }
    });
}); };
function createComponent(_a) {
    var type = _a.type, name = _a.name;
    var fileEncoding = { encoding: 'utf-8', flag: 'rs' };
    var newType = get_settings_1.isUsingHooks ? "".concat(type, "-hooks") : type;
    return new Promise(function (resolve) {
        console.log("Creating ".concat(type, " ").concat(name).concat(get_settings_1.isUsingHooks ? ' with hooks' : '', "..."));
        (0, fs_1.readdirSync)((0, path_1.join)(__dirname, "/../file_package_repo/".concat(newType))).forEach(function (file, i) {
            var pascalName = (0, change_case_1.pascalCase)(name);
            var camelName = (0, change_case_1.camelCase)(name);
            var kebabName = (0, change_case_1.paramCase)(name);
            if (!i)
                (0, child_process_1.execSync)("mkdir ".concat(name));
            var isFolder = file.lastIndexOf('.') === -1;
            if (isFolder)
                (0, child_process_1.execSync)("mkdir ".concat(name, "/").concat(file.replace(/Placeholder_pascal/g, pascalName)));
            else
                (0, child_process_1.execSync)("touch ./".concat(name, "/").concat(file.replace(/Placeholder_pascal/g, pascalName)));
            // @ts-ignore
            var data = (0, fs_1.readFileSync)((0, path_1.join)(__dirname, "/../file_package_repo/".concat(newType, "/").concat(file)), fileEncoding);
            (0, fs_1.writeFileSync)("./".concat(name, "/").concat(file.replace(/Placeholder_pascal/g, pascalName)), data.replace(/Placeholder_kebab/g, kebabName).replace(/Placeholder_camel/g, camelName).replace(/Placeholder_pascal/g, pascalName));
            resolve(true);
        });
    });
}
function finish() {
    console.log('Finished building.\n');
    process.exit();
}
function errorHandler(msg) {
    console.log(msg);
    process.exit();
}
initTemplateModule()
    .then(createComponent, errorHandler)
    .then(finish);
