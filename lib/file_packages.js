"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var filePackages = {
    'component': { folder: 'component', hasHooks: true },
    'component-no-connect': { folder: 'component-hooks-no-connect', hasHooks: true },
    'component-stateless': { folder: 'component-stateless', hasHooks: false },
};
exports.default = filePackages;
