"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = require("chalk");
var modules_1 = require("../modules");
var file_packages_1 = __importDefault(require("../file_packages"));
var get_spaces_1 = __importDefault(require("../utils/get-spaces"));
exports.default = (function () {
    var commandUsed = modules_1.settings.value('commandUsed');
    var list = Object.assign({ Component: 'Hooks' }, file_packages_1.default);
    console.log(("\nSome components also have a hooks type, which is shown in the table below.\nTo activate this type, run ".concat((0, chalk_1.red)("".concat(commandUsed, " config hooks true")), ".\n\nThe following templates are available:\n\n").concat(Object.keys(list).map(function (tpl, i) { return "".concat(!i ? (0, chalk_1.blue)(tpl) : tpl).concat((0, get_spaces_1.default)(list, tpl)).concat(!i ? (0, chalk_1.blue)(list[tpl]) : list[tpl].hasHooks, "\n"); }), "\n")).replace(/,/g, ''));
    process.exit();
});
