"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = require("chalk");
exports.default = (function () {
    console.log("\nThis module will help you to easily create new React components.\n    \n".concat((0, chalk_1.blue)('Each component includes the following files:'), "\n- Main component\n- Test\n- Styling\n\n").concat((0, chalk_1.blue)('The following commands are available:'), "\n--help     --h            Open this help overview\n--version  --v            Show package version\n--list     --ls           Show a list of available templates\n--config   config         See below\n\"tpl-name  el-name\"       Choose a template and create a folder with the desired element name (CamelCase)\n\n").concat((0, chalk_1.blue)('The following config commands are available'), "\nconfig                    Show current settings\nconfig reset              Reset config to default settings\nconfig <key> <value>      Set config 'key' to 'value'         \n\n").concat((0, chalk_1.blue)('Developer:'), "\nRichard van Hees          richard@habitssoftware.nl\n"));
    process.exit();
});
