"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.configFunc = exports.versionFunc = exports.readmeFunc = exports.listFunc = void 0;
var list_1 = __importDefault(require("./list"));
exports.listFunc = list_1.default;
var read_me_1 = __importDefault(require("./read-me"));
exports.readmeFunc = read_me_1.default;
var version_1 = __importDefault(require("./version"));
exports.versionFunc = version_1.default;
var config_1 = __importDefault(require("./config"));
exports.configFunc = config_1.default;
