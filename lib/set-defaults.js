"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var modules_1 = require("./modules");
var set_config_1 = __importDefault(require("./utils/set-config"));
var config_1 = __importDefault(require("./config"));
exports.default = (function () {
    var usedBefore = modules_1.settings.value('usedBefore');
    if (!usedBefore) {
        (0, set_config_1.default)(config_1.default, { isReset: true });
        modules_1.settings.setValue("usedBefore", true);
    }
});
