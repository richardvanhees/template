import { Confirm } from '../modules';

export default (query: string): Promise<string> => {
  const confirm = new Confirm(query);

  return new Promise((resolve) => {
    confirm.ask((answer: string) => resolve(answer));
  });
};