import { settings } from '../modules';
import setDefaults from '../set-defaults';
import getCommand from '../get-command';

// Init
settings.init({ appName: 'React Template Engine', reverseDNS: 'com.bar.foo' });
setDefaults();
settings.setValue('commandUsed', getCommand());

export const commandUsed = settings.value('commandUsed');
export const isUsingHooks = settings.value('hooks');
export const isUsingRedux = settings.value('redux');
export const isUsingJss = settings.value('jss');
export const isUsingTypeScript = settings.value('typescript');
