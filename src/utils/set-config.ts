import { blue, red } from 'chalk';
import { settings } from '../modules';

import getBoolean from './transform-to-boolean';

export default (config: { [key: string]: any }, opts?: { [key: string]: any }) => {
  const options = opts || {};
  const configKeys = Object.keys(config);

  configKeys.forEach((key, i) => {
    settings.setValue(key, getBoolean(config[key]), false);
    !options.isReset && console.log(`${blue(key)} is now ${red(config[key])}`);
    if (i + 1 === configKeys.length) console.log('');
  });

  options.isReset && console.log(`The ${blue('default values')} have been restored.`);
};