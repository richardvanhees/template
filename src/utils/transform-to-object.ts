interface SettingsObject {
  [key: string]: string;
  hooks: string;
  jss: string;
  redux: string;
  TypeScript: string;
}

export default (arr: string[]): SettingsObject => {
  const newObj = {} as SettingsObject;

  arr.forEach((key: string, i: number) => {
    if (!(i % 2)) {
      newObj[key] = arr[i + 1];
    }
  });

  return newObj;
};
