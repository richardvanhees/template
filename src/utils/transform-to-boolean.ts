export default (val: string): boolean | string => {
  if (val === 'true') return true;
  if (val === 'false') return false;
  return val;
};