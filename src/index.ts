#!/usr/bin/env node --harmony

import { join as pathJoin } from 'path';
import { readdirSync, readFileSync, writeFileSync } from 'fs';
import { execSync } from 'child_process';
import minimist from 'minimist';
import { pascalCase, camelCase, paramCase } from 'change-case';
import { blue } from 'chalk';

import filePackages from './file_packages';
import { listFunc, readmeFunc, versionFunc, configFunc } from './cli-funcs';
import { commandUsed, isUsingHooks, isUsingRedux } from './utils/get-settings';

const options = minimist(process.argv.slice(2));

const initTemplateModule = async (): Promise<{ type: string, name: string } | string> => {
  if (options.list || options.ls || (options.l && options.s)) await listFunc();
  if (options.help || options.h) await readmeFunc();
  if (options.version || options.v) await versionFunc();
  if (options.config || options._[0] === 'config') await configFunc();

  return new Promise((resolve, reject) => {
    const type: string = options._[0];
    const name: string = options._[1];

    if (!options._.length) reject(`\nNo arguments found. Run ${blue(`${commandUsed} --help`)} for more information.\n`);
    if (!name) reject(`\nUsage: ${blue(`${commandUsed} \<template name\> \<element name\>`)}\nRun ${blue(`${commandUsed} --help`)} for more information.\n`);
    if (!filePackages[type]) reject(`\nTemplate ${type} does not exist.\nRun ${blue(`${commandUsed} -ls`)} for all templates.\n`);
    if (isUsingHooks && !filePackages[type].hasHooks) reject(`\nTemplate ${type} does not support hooks.\n`);

    resolve({ type, name });
  });
};

function createComponent({ type, name }: { type: string, name: string }) {
  const fileEncoding = { encoding: 'utf-8', flag: 'rs' };
  const newType = isUsingHooks ? `${type}-hooks` : type;

  return new Promise(resolve => {
    console.log(`Creating ${type} ${name}${isUsingHooks ? ' with hooks' : ''}...`);

    readdirSync(pathJoin(__dirname, `/../file_package_repo/${newType}`)).forEach((file, i) => {
      const pascalName = pascalCase(name);
      const camelName = camelCase(name);
      const kebabName = paramCase(name);
      if (!i) execSync(`mkdir ${name}`);
      const isFolder = file.lastIndexOf('.') === -1;

      if (isFolder) execSync(`mkdir ${name}/${file.replace(/Placeholder_pascal/g, pascalName)}`);
      else execSync(`touch ./${name}/${file.replace(/Placeholder_pascal/g, pascalName)}`);

      // @ts-ignore
      const data = readFileSync(pathJoin(__dirname, `/../file_package_repo/${newType}/${file}`), fileEncoding);
      writeFileSync(`./${name}/${file.replace(/Placeholder_pascal/g, pascalName)}`, data.replace(/Placeholder_kebab/g, kebabName).replace(/Placeholder_camel/g, camelName).replace(/Placeholder_pascal/g, pascalName));
      resolve(true);
    });
  });
}

function finish() {
  console.log('Finished building.\n');
  process.exit();
}

function errorHandler(msg: string) {
  console.log(msg);
  process.exit();
}

initTemplateModule()
  .then(createComponent, errorHandler)
  .then(finish);
