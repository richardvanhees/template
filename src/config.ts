// Default config

export default {
  hooks: false,
  jss: false,
  redux: false,
  TypeScript: false,
};