/**
 * Create a new `Confirm` prompt, with the given `question`.
 */
export default class Confirm {
  constructor(query: string);
  /**
   * Run the prompt with the given `callback` function.
   *
   * ```js
   * var Prompt = require('prompt-base');
   * var prompt = new Prompt({
   *   name: 'name',
   *   message: 'What is your name?'
   * });
   *
   * prompt.ask(function(answer) {
   *   console.log(answer);
   * });
   * ```
   * @param {Function} `callback`
   * @return {undefined}
   */
  ask: (callback: (answer: string) => void) => void;
}
