import type { PathOrFileDescriptor } from 'fs';

/** @function init
 @description initializes the settings store based on the given options.
 @param {
   electronApp?: boolean,
   fileName?: string,
   enableReloading?: boolean
 } opts
 */
export function init(opts?: { appName: string, reverseDNS?: string, electronApp?: boolean, fileName?: string, enableReloading?: boolean }): void;

// ============================================================================
/** @function value
 @description Returns overridden, stored or default value for given key.
 Defaults passed as defaultValue to this function take precedence over
 those set with loadDefaults/setDefaults.
 All returned objects can be safely manipulated without altering the store
 @param {string|string[]} key [key] - can be a string with nested keys separated by "." or an array or nested key strings
 @param defaultValue - if no value has been set for the provided key, defaultValue will be returned. Takes precedence over previously set default values.
 */
export function value(key: string | string[], defaultValue?: string): string;

// ============================================================================
/** @function setValue
 @description Changes the value of key to value and updates the settings file.
 @param {string|string[]} key - can be a string (also with nested keys, separated by ".") or an array of nested key strings
 @param {any} value - the new value for key. Can be any type. If `value` is an object, nested keys will also be stored.
 @param {boolean} merge [merge=false] - Use when passing an object for *value*. Enables or disables merging of new *value* object with previously stored values.
 */
export function setValue(key: string | string[], value: any, merge?: boolean): void;

// ============================================================================
/** @function override
 @description Changes the value of *key* to *value* without updating the settings file.
 @param {string|string[]} key - can be a string (also with nested keys, separated by ".") or an array of nested key strings
 @param {any} value - the new value for *key*. Can be any type. If *value* is an object, nested keys will also be stored. If *value* is *undefined*, it will be treated as not overridden.
 */
export function override(key: string | string[], value: any): void;

// ============================================================================
/** @function setDefaults
 @description Set all default values at once.
 @param {object} defaultValues - object containing all desired default values
 */
export function setDefaults(defaultValues: { [key: string]: any }): void;

// ============================================================================
/** @function loadDefaults
 @description Reads default settings from a JSON file
 @param {PathOrFileDescriptor} fileName - the complete path to a JSON file containing settings default values
 */
export function loadDefaults(fileName: PathOrFileDescriptor): void;

// ============================================================================
/** @function all
 @description Returns all currently set values. Does not include defaults or overrides.
 */
export function all(): { [key: string]: any };

// ============================================================================
/** @function clear
 @description Removes all entries. Does not reset defaults or overrides.
 */
export function clear(): void;
