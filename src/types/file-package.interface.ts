interface FilePackage {
  folder: string;
  hasHooks: boolean;
}

export default FilePackage;
