import type FilePackage from './types/file-package.interface';

interface FilePackageList {
  [key: string]: FilePackage;
}

const filePackages: FilePackageList = {
  'component': { folder: 'component', hasHooks: true },
  'component-no-connect': { folder: 'component-hooks-no-connect', hasHooks: true },
  'component-stateless': { folder: 'component-stateless', hasHooks: false },
};

export default filePackages;
