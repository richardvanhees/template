import { settings } from './modules';

import setConfig from './utils/set-config';
import defaultConfig from './config';

export default () => {
  const usedBefore = settings.value('usedBefore');

  if (!usedBefore) {
    setConfig(defaultConfig, { isReset: true });
    settings.setValue("usedBefore", true);
  }
};
