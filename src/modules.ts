// @ts-nocheck
import settings from 'settings-store';
import Confirm from 'prompt-confirm';

export { settings, Confirm };
