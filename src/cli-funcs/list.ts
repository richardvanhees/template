import { red, blue } from 'chalk';
import { settings } from '../modules';
import filePackages from '../file_packages';
import getSpaces from '../utils/get-spaces';

export default () => {
  const commandUsed = settings.value('commandUsed');
  const list = Object.assign({ Component: 'Hooks' }, filePackages);

  console.log((`
Some components also have a hooks type, which is shown in the table below.
To activate this type, run ${red(`${commandUsed} config hooks true`)}.

The following templates are available:

${Object.keys(list).map((tpl, i) => `${!i ? blue(tpl) : tpl}${getSpaces(list, tpl)}${!i ? blue(list[tpl]) : list[tpl].hasHooks}\n`)}
`).replace(/,/g, ''));
  process.exit();
}
;