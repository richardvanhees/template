import minimist from 'minimist';
import { blue, red } from 'chalk';
import { omit } from 'ramda';
import { settings } from '../modules';

import askQuestion from '../utils/ask-question';
import getSpaces from '../utils/get-spaces';
import setConfig from '../utils/set-config';
import transformToObject from '../utils/transform-to-object';
import defaultConfig from '../config';

export default () => new Promise(async () => {
  const cliInput = minimist(process.argv.slice(3))._;
  const commandUsed = settings.value('commandUsed');

  // template config
  if (!cliInput.length) {
    const currentSettings = omit(['commandUsed', 'usedBefore'], settings.all());
    console.log('\nCurrent settings:');

    Object.keys(currentSettings).forEach(setting =>
      console.log(`${setting}:${getSpaces(currentSettings, setting)}${blue(currentSettings[setting])}`),
    );
    console.log(`\nYou can change these settings by running "${red(`${commandUsed} config <key> <value>`)}".\n`);
    process.exit();
  }

  // template config reset
  if (cliInput[0] === 'reset') {
    const ans = await askQuestion(`Are you sure you want to ${red('reset')} all settings to their ${red('default values')}?`);
    if (ans) {
      settings.clear();
      setConfig(defaultConfig, { isReset: true });
    }
    process.exit();
  }

  // template config <key> <value> <key> <value>...
  setConfig(transformToObject(cliInput));
  process.exit();
});
