import packageJson from '../../package.json';

export default () => {
  console.log(`v${packageJson.version}`);
  process.exit();
};
