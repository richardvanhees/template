import React, { useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import useStyles from './styles'

const Placeholder_pascalComponent = props => {
  const { className } = props
  const [state, setState] = useState({})
  const c = useStyles()

  return (
    <div className={clsx(c.root, className)}>

    </div>
  )
}

Placeholder_pascalComponent.propTypes = {
  className: PropTypes.string
}

Placeholder_pascalComponent.defaultProps = {
  className: ''
}

export default Placeholder_pascalComponent
