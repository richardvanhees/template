import React, { useState } from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'
import useStyles from './styles'

const Placeholder_pascal = props => {
  const { className } = props
  const [state, setState] = useState({})
  const c = useStyles()

  return (
    <div className={clsx(c.root, className)}>

    </div>
  )
}

Placeholder_pascal.propTypes = {
  className: PropTypes.string
}

Placeholder_pascal.defaultProps = {
  className: ''
}

export default Placeholder_pascal
